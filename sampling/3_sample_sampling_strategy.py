import os
import torch
import argparse
from tqdm import tqdm

from tools.simulator import simulate_observations
from tools.sampler import topo_random_sampler
from tools.writer import save_pickle
import config

cfg = config.Config()

parser = argparse.ArgumentParser()
# Training parameters
parser.add_argument('-N', '--subtree-size', default=6, type=int)
args = parser.parse_known_args()[0]
N = args.subtree_size
torch.manual_seed(N)

chunk_len = 30
events_per_amp = 12
save_path = cfg.dir.samp_strat_data
if not os.path.exists(save_path):
    os.mkdir(save_path)

# Sampling the smartly sampled training data
X, As, aff = simulate_observations(N=N, events_per_edge=events_per_amp, chunk_len=chunk_len, savepath=save_path,
                                   type='smart', add_name='train_smart', num_samples=500)
# Reusing the true topologies and the generated time series for the randomly sampled test
new_As = []
for A in tqdm(As, desc="Adjusting matrices"):
    new_A = A.clone()
    new_A[1:, :N, :N] = topo_random_sampler(N=N, num_samples=new_A.shape[0]-1, type='mix', with_replacement=False)
    new_As.append(new_A)
add_name = 'train_random'
# Saving all
save_pickle(save_path.joinpath(f"{N}_{events_per_amp}_time_series{'' if add_name is None else '_' + add_name}"), X)
save_pickle(save_path.joinpath(f"{N}_{events_per_amp}_affected{'' if add_name is None else '_' + add_name}"), aff)
save_pickle(save_path.joinpath(f"{N}_{events_per_amp}_topologies{'' if add_name is None else '_' + add_name}"), new_As)

# Sampling the validation data
if N == 6:
    simulate_observations(N=N, events_per_edge=events_per_amp, chunk_len=chunk_len, savepath=save_path,
                          type='full', add_name='validation', num_samples=500)
else:
    simulate_observations(N=N, events_per_edge=events_per_amp, chunk_len=chunk_len, savepath=save_path,
                          type='mix', add_name='validation', num_samples=500, parallellize=True)
