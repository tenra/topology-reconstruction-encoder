import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import seaborn as sns

import config

cfg = config.Config()

tree_sizes = [4, 5, 6]
colors = ['#206e96', '#d44f49', '#7fba66', '#f57e2a']
dcolors = ['#0a212d', '#451310', '#253c1b']
linestyles = ['-', ':', '--', '-.']
results_4 = pd.read_csv(cfg.dir.results.joinpath('alpha_grid_search_4.csv'))
results_5 = pd.read_csv(cfg.dir.results.joinpath('alpha_grid_search_5.csv'))
results_6 = pd.read_csv(cfg.dir.results.joinpath('alpha_grid_search_6.csv'))
results = [results_4[results_4['prop_ones'] >= 0.00],
           results_5[results_5['prop_ones'] >= 0.00],
           results_6[results_6['prop_ones'] >= 0.00]]
px = 1 / plt.rcParams['figure.dpi']  # pixel in inches
import matplotlib as mpl

type = 'median'  # 'median'
quantiles = [0.25, 0.75]

mpl.rc('font', family="Times")
plt.rcParams['legend.title_fontsize'] = 14
plt.rcParams['mathtext.fontset'] = 'cm'

# %% Example data plot
X = pd.read_csv(cfg.dir.data.joinpath('example_data.csv'), header=None).values
t = np.arange(96)*0.25
idx = [10, 4, 0, 5]
cidx = [3, 2, 1, 0]

fig, ax = plt.subplots(1, 1, figsize=(10, 3))
for i, x in enumerate(X[idx]):
    ax.plot(t, x, linewidth=3, c=colors[cidx[i]])
ax.spines[['left', 'right', 'top', 'bottom']].set_visible(False)
ax.set_xlabel('Hours', fontsize=14)
ax.set_ylabel('Min. MER', fontsize=14)
ax.tick_params(axis='x', labelsize=12)
ax.tick_params(axis='y', labelsize=12)

fig.show()
fig.savefig(cfg.dir.plots.joinpath('example_data.png'), bbox_inches='tight', dpi=200)

# %% Trying to do the grouped box plots
results_4['N'] = 4
results_5['N'] = 5
results_6['N'] = 6

results_all = pd.concat([results_4, results_5, results_6])
results_all['mean_events/amp'] = (results_all['mean_events/amp'] / 2).astype('int')

fig, ax = plt.subplots(1, 1, figsize=[5, 5])
sns.boxplot(results_all, x='mean_events/amp', y='topo_unique_acc', hue='N', ax=ax, palette=colors, linewidth=1.5,
            saturation=1)
xlim = ax.get_xlim()
ylim = ax.get_ylim()
ax.plot([-0, 12], [1.0, 1.0], c='gray', linestyle='dashed', zorder=0)
ax.set_xlim(xlim)
for i, cont in enumerate(ax.containers):
    for box in cont:
        box.box.set(linewidth=1)
        box.median.set(linewidth=3, color=dcolors[i])

ax.spines[['left', 'right', 'top', 'bottom']].set_visible(False)
ax.set_xlabel('Mean number of events per internal edge', fontsize=14)
ax.set_ylabel('True Topology Accuracy', fontsize=14)
ax.tick_params(axis='x', labelsize=12)
ax.tick_params(axis='y', labelsize=12)
legend = ax.legend(title='$|\mathit{N}|$', fontsize=14)
legend.remove()
fig.show()
fig.savefig(cfg.dir.plots.joinpath('num_events_topo_figure.png'), bbox_inches='tight', dpi=200)

fig, ax = plt.subplots(1, 1, figsize=[5, 5])
sns.boxplot(results_all, x='mean_events/amp', y='event_acc_mean', hue='N', ax=ax, palette=colors, linewidth=1.5, saturation=1)
xlim = ax.get_xlim()
ax.plot([-0, 12], [1.0, 1.0], c='gray', linestyle='dashed', zorder=0)
ax.set_xlim(xlim)
ax.set_ylim(ylim)
for i, cont in enumerate(ax.containers):
    for box in cont:
        box.box.set(linewidth=1)
        box.median.set(linewidth=3, color=dcolors[i])
ax.spines[['left', 'right', 'top', 'bottom']].set_visible(False)
ax.set_xlabel('Mean number of events per internal edge', fontsize=14)
ax.set_ylabel('Event  Accuracy', fontsize=14)
ax.tick_params(axis='x', labelsize=12)
ax.tick_params(axis='y', labelsize=12)
legend = ax.legend(title='$|\mathit{N}|$', fontsize=14)
fig.show()
fig.savefig(cfg.dir.plots.joinpath('num_events_event_figure.png'), bbox_inches='tight', dpi=200)

# %% Results for ideal case
results_4 = pd.read_csv(cfg.dir.results.joinpath('ideal_conditions_4.csv'))
results_5 = pd.read_csv(cfg.dir.results.joinpath('ideal_conditions_5.csv'))

print(f"Removed {[len(res) - 20 for res in [results_4, results_5]]} observations, respectively, for trees of size 4 and 5")
results = [results_4[results_4['prop_ones'] > 0],
           results_5[results_5['prop_ones'] > 0]]

# %% Data for the table of performance
ideal_num_events = 20
this_topo, this_event = [], []
for result in results:  # Running over subtree-sizes
    this = result[(result['mean_events/amp'] == ideal_num_events)]
    this_topo.append(this['topo_unique_acc'].values)
    this_event.append(this['event_acc_mean'].values)

print(f"{'N': ^10s}{'Topology accuracy': ^40s}{'Event accuracy': ^40s}\n"
      f"{'': ^10s}{'Mean': ^20s}{'Std.': ^20s}{'Mean': ^20s}{'Std.': ^20s}"
      f"\n{'':-^100s}")
for n, topo, event in zip([4, 5], this_topo, this_event):
    print(f"{n: ^10d}{np.mean(topo*100): ^20.2f}{np.std(topo*100): ^20.2f}{np.mean(event*100): ^20.2f}{np.std(event*100): ^20.2f}")

# %% Inspecting the multiprop data
results_5 = pd.read_csv(cfg.dir.results.joinpath('multi_prop_5.csv'))
results_4 = pd.read_csv(cfg.dir.results.joinpath('multi_prop_4.csv'))
results = [results_4[(results_4['prop_ones'] >= 0.0)],
           results_5[results_5['prop_ones'] >= 0.0]]

prop_list = np.sort(results_5.multiprop.unique())
topo_data, event_data = [], []
for result in results:
    this_topo, this_event = [], []
    for mp in prop_list:
        this = result[result['multiprop'] == mp]
        this_topo.append(this['topo_unique_acc'].values)
        this_event.append(this['event_acc_mean'].values)
    topo_data.append(this_topo)
    event_data.append(this_event)

topo_fig, topo_ax = plt.subplots(1, 1, figsize=(5, 5))
event_fig, event_ax = plt.subplots(1, 1, figsize=(5, 5))
for i, (this_topo_data, this_event_data) in enumerate(zip(topo_data, event_data)):
    if type == 'mean':
        topo_med = [np.mean(x) for x in this_topo_data]
        topo_err = np.tile(np.stack([np.std(x) for x in this_topo_data if len(x) > 0])[None, :], (2, 1))
    elif type == 'median':
        topo_med = [np.median(x) for x in this_topo_data]
        topo_err = np.stack([np.quantile(x, q=quantiles) for x in this_topo_data], axis=1)
        #topo_min = [np.min(x) for x in this_topo_data if len(x) > 0]
        #topo_ax.scatter(y=topo_min, x=(prop_list[:len(topo_min)]) + i * 0.7,
        #                linewidths=2, marker='x', s=70, zorder=2, c=colors[i])
    topo_ax.scatter(y=topo_med,
                    x=prop_list + 0.3,
                    marker='o', s=70, zorder=2, c=colors[i])
    (_, caps, _) = topo_ax.errorbar(y=topo_med,
                                    x=prop_list + i * 0.6,
                                    yerr=topo_err if type == 'mean' else np.abs(topo_err - topo_med),
                                    marker='o', capsize=10, label=f"{tree_sizes[i]}", linewidth=2, zorder=1,
                                    c=colors[i], linestyle=linestyles[i])
    for cap in caps:
        cap.set_markeredgewidth(2)
    if type == 'mean':
        event_med = [np.mean(x) for x in this_event_data]
        event_err = np.tile(np.stack([np.std(x) for x in this_event_data if len(x) > 0])[None, :], (2, 1))
    elif type == 'median':
        event_med = [np.median(x) for x in this_event_data]
        event_err = np.stack([np.quantile(x, q=quantiles) for x in this_event_data], axis=1)
    event_ax.scatter(y=event_med,
                     x=prop_list + 0.3,
                     marker='o', s=70, zorder=2, c=colors[i])
    (_, caps, _) = event_ax.errorbar(y=event_med,
                                     x=prop_list + i * 0.6,
                                     yerr=event_err if type == 'mean' else np.abs(event_err - event_med),
                                     marker='o', capsize=10,
                                     label=f"{tree_sizes[i]}", linewidth=2, zorder=1,
                                     c=colors[i], linestyle=linestyles[i])
    for cap in caps:
        cap.set_markeredgewidth(2)
#  topo_ax.set_title('Topology accuracy as function of proportion of multiple events')
topo_ax.plot(prop_list[[0, -1]], [1.0, 1.0], c='gray', linestyle='dashed', zorder=0)
#if type == 'median':
#    topo_ax.scatter(y=-10, x=mea_list[0] / 2, linewidths=2, marker='x', s=70, zorder=2, c='black', label='Min')
topo_ax.set_xlabel('Proportion of multi-location faults (%)', fontsize=14)
topo_ax.set_ylabel('True Topology Accuracy', fontsize=14)
topo_ax.tick_params(axis='y', labelsize=12)
topo_ax.tick_params(axis='x', labelsize=12)
topo_ax.set_ylim(-0.05, 1.05)
#topo_ax.legend(fontsize=12)
topo_ax.spines[['left', 'right', 'top', 'bottom']].set_visible(False)

event_ax.plot(prop_list[[0, -1]], [1.0, 1.0], c='gray', linestyle='dashed', zorder=0)
#if type == 'median':
#    event_ax.scatter(y=-10, x=mea_list[0] / 2, linewidths=2, marker='x', s=70, zorder=2, c='black', label='Min')
event_ax.set_ylim(-0.05, 1.05)
#  event_ax.set_title('Event accuracy as function of proportion of multiple events')
event_ax.set_xlabel('Proportion of multi-location faults (%)', fontsize=14)
event_ax.set_ylabel('Event Accuracy', fontsize=14)
event_ax.tick_params(axis='y', labelsize=12)
event_ax.tick_params(axis='x', labelsize=12)
event_ax.legend(title='$\mathit{|N|}$', fontsize=12, loc='lower right')
event_ax.spines[['left', 'right', 'top', 'bottom']].set_visible(False)

topo_fig.show()
event_fig.show()
topo_fig.savefig(cfg.dir.plots.joinpath('multiprop_topo_figure.png'), bbox_inches='tight', dpi=200)
event_fig.savefig(cfg.dir.plots.joinpath('multiprop_event_figure.png'), bbox_inches='tight', dpi=200)

# %% The general encoder performance
results = pd.read_csv(cfg.dir.results.joinpath('general_encoder.csv'))
results = results[results['prop_ones'] > 0]

pars_acc = results['topo_unique_acc'].values
event_acc = results['event_acc_mean'].values

print(f"Unique parsimony accuracy: {pars_acc.mean()*100:.2f} (+- {(pars_acc*100).std():.2f})")
print(f"Event accuracy           : {event_acc.mean()*100:.2f} (+- {(event_acc*100).std():.2f})")


# %% Making the sampling plots as box-plots also
tree_sizes = [6, 7, 9, 15]
all_results = None

figs, axes = [], []
for ts in tree_sizes:
    results_random = pd.read_csv(cfg.dir.results.joinpath(f'sampling_strat/random_{ts}.csv'))
    results_random['strategy'] = 'random'

    results_smart = pd.read_csv(cfg.dir.results.joinpath(f'sampling_strat/smart_{ts}.csv'))
    results_smart['strategy'] = 'smart'

    these_results = pd.concat([results_random, results_smart])
    these_results = these_results[these_results['prop_ones'] > 0]

    fig, ax = plt.subplots(1, 1, figsize=(5, 5))
    sns.boxplot(these_results, x='num_samples', y='topo_unique_acc', hue='strategy', ax=ax, palette=colors, linewidth=1.5,
                saturation=1)
    figs.append(fig)
    axes.append(ax)

figs[3].show()

# %% Loading the sampling strategy results
type = 'mean'
tree_sizes = [6, 7, 9, 15]
results_random = [pd.read_csv(cfg.dir.results.joinpath(f'sampling_strat/random_{ts}.csv')) for ts in tree_sizes]
results_smart = [pd.read_csv(cfg.dir.results.joinpath(f'sampling_strat/smart_{ts}.csv')) for ts in tree_sizes]


topo_data, event_data, k_lists = [], [], []
for ts, result_rand, result_smart in zip(tree_sizes, results_random, results_smart):
    result_rand = result_rand[result_rand['prop_ones'] >= 0.00]
    result_smart = result_smart[result_smart['prop_ones'] >= 0.00]
    this_topo_rand, this_topo_smart = [], []
    this_event_rand, this_event_smart = [], []
    k_list = np.sort(result_rand['num_samples'].unique())
    for k in k_list:
        this_rand = result_rand[result_rand['num_samples'] == k]
        this_smart = result_smart[result_smart['num_samples'] == k]
        this_topo_rand.append(this_rand['topo_unique_acc'].values)
        this_topo_smart.append(this_smart['topo_unique_acc'].values)
        this_event_rand.append(this_rand['event_acc_mean'].values)
        this_event_smart.append(this_smart['event_acc_mean'].values)
    topo_data.append((this_topo_rand, this_topo_smart))
    event_data.append((this_event_rand, this_event_smart))
    k_lists.append(k_list)

alpha = 0.015
figs, axes = [], []
for i, ((this_topo_rand, this_topo_smart), (this_event_rand, this_event_smart), k_list) in enumerate(zip(event_data, topo_data, k_lists)):
    fig, ax = plt.subplots(1, 1, figsize=(5, 5))
    if type == 'mean':
        topo_med_rand = [np.mean(x) for x in this_topo_rand]
        topo_med_smart = [np.mean(x) for x in this_topo_smart]
        topo_err_rand = np.tile(np.array([np.std(x) for x in this_topo_rand]).reshape((-1, 1)), 2).T
        topo_err_smart = np.tile(np.array([np.std(x) for x in this_topo_smart]).reshape((-1, 1)), 2).T
    else:
        topo_med_rand = [np.median(x) for x in this_topo_rand]
        topo_med_smart = [np.median(x) for x in this_topo_smart]
        np.stack([np.quantile(x, q=quantiles) for x in this_event_data], axis=1)
        topo_err_rand = np.stack([np.quantile(x, q=quantiles) for x in this_topo_rand], axis=1)
        topo_err_smart = np.stack([np.quantile(x, q=quantiles) for x in this_topo_smart], axis=1)
    ax.scatter(y=topo_med_rand,
               x=k_list + 0.0,
               marker='o', s=70, zorder=2, c=colors[0])
    (_, caps, _) = ax.errorbar(y=topo_med_rand,
                               x=k_list * (1 - alpha),
                               yerr=topo_err_rand if type == 'mean' else np.abs(topo_err_rand - topo_med_rand),
                               marker='o', capsize=10, label=f"Random sampling (± s.d.)", linewidth=2, zorder=1, c=colors[0])
    for cap in caps:
        cap.set_markeredgewidth(2)

    ax.scatter(y=topo_med_smart,
               x=k_list + 0.0,
               marker='o', s=70, zorder=2, c=colors[1])
    (_, caps, _) = ax.errorbar(y=topo_med_smart,
                               x=k_list * (1 + alpha * k_list[-1]/45),
                               yerr=topo_err_smart if type == 'mean' else np.abs(topo_err_smart - topo_med_smart),
                               marker='o', capsize=10, label=f"Smart sampling (± s.d.)", linewidth=2, zorder=1, c=colors[1])
    for cap in caps:
        cap.set_markeredgewidth(2)
    #axes[i].boxplot(this_topo_data)

    #  topo_ax.set_title('Topology accuracy as function of proportion of multiple events')
    ax.plot(k_list[[0, -1]], [1.0, 1.0], c='gray', linestyle='dashed', zorder=0)
    ax.set_title(f'N = {tree_sizes[i]}', fontsize=16)
    ax.set_xlabel('Number of samples', fontsize=14)
    if i == 0:
        ax.set_ylabel('True Topology Accuracy', fontsize=14)
    else:
        ax.set_ylabel('Estimated Topology Accuracy', fontsize=14)
    ax.tick_params(axis='y', labelsize=12)
    ax.tick_params(axis='x', labelsize=12)
    ax.spines[['left', 'right', 'top', 'bottom']].set_visible(False)
    ax.set_xscale('log')
    if tree_sizes[i] == 9:
        k_list = k_list[k_list != 26]
    elif tree_sizes[i] == 6:
        k_list = k_list[k_list != 16]
    ax.set_xticks(k_list, k_list.astype('int'))
    if i == 0:
        ax.legend(fontsize=12)
    fig.savefig(cfg.dir.plots.joinpath(f'sampling_figure_{tree_sizes[i]}.png'), bbox_inches='tight', dpi=200)
    figs.append(fig)
    axes.append(ax)


figs[3].show()

# %% Make a example data plot
import pickle
file_path = cfg.dir.data.joinpath('4_10_time_series.pkl')
with open(file_path, 'rb') as file:
    X = pickle.load(file)
file_path = cfg.dir.data.joinpath('4_10_affected.pkl')
with open(file_path, 'rb') as file:
    aff = pickle.load(file)
file_path = cfg.dir.data.joinpath('4_10_topologies.pkl')
with open(file_path, 'rb') as file:
    A = pickle.load(file)

idx = 0
X = X[idx]
aff = aff[idx]
A = A[idx]

fig, (ax1, ax2, ax3) = plt.subplots(3, 1, figsize=(7, 5), height_ratios=(10, 10, 1))
ax1.plot(X[20, 0, 2200:2900].numpy().T, linewidth=3, c=colors[0])
ax2.plot(X[15, 0, 2200:2900].numpy().T, linewidth=3, c=colors[0])
ax3.plot([105, 300], [3, 3], linewidth=5, c=colors[1])
ax3.plot([300, 495], [3, 3], linewidth=5, c=colors[2])
ax3.set_xlim(-35, 735)
#ax2.plot([100, 300], [3,3], linewidth=3)

#ax1.set_ylabel('Feature value', fontsize=14)
#ax2.set_ylabel('Feature value', fontsize=14)
#ax3.set_xlabel('Time', fontsize=14)
ax1.tick_params(axis='y', labelsize=12)
ax3.tick_params(axis='x', labelsize=12)
ax2.tick_params(axis='y', labelsize=12)
ax1.set_yticks([-2, 0, 2, 4], [-2, 0, 2, 4], fontsize=12)
ax2.set_yticks([-2, 0, 2, 4], [-2, 0, 2, 4], fontsize=12)

ax1.spines[['left', 'right', 'top', 'bottom']].set_visible(False)
ax2.spines[['left', 'right', 'top', 'bottom']].set_visible(False)
ax3.spines[['left', 'right', 'top']].set_visible(False)
ax1.get_xaxis().set_visible(False)
ax2.get_xaxis().set_visible(False)
ax3.get_yaxis().set_visible(False)
fig.show()
fig.savefig(cfg.dir.plots.joinpath('example_sim_data.png'), bbox_inches='tight', dpi=200)
