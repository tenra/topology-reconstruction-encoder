from tools.data import upper_vector_to_full
import config
import pickle

import pandas as pd
import torch

cfg = config.Config()


def load_permutations(subtree_size: int = 4):
    """
    Loading permuations specified in a short format and turning them into a stack of adjacency_matrices

    :return:
    """
    AUs = torch.tensor(pd.read_csv(cfg.dir.root.joinpath(f'utility/permutations_{subtree_size}.csv'),
                                   header=None, index_col=None).values)
    A = []
    for AU in AUs:
        A.append(upper_vector_to_full(AU))
    return torch.stack(A)


def load_saved_data(subtree_size=None, num_events_per_edge=None, added_name=None, multiprop=False, grid_search=False,
                    sampling=False, ideal_conditions=False, const_len=False, general_encoder=False):
    num_events_per_edge = cfg.sim.num_events_per_amp if num_events_per_edge is None else num_events_per_edge

    if multiprop:
        datapath = cfg.dir.multi_prop_data
    elif grid_search:
        datapath = cfg.dir.num_events_grid_data
    elif const_len:
        datapath = cfg.dir.num_events_T1000
    elif sampling:
        datapath = cfg.dir.samp_strat_data
    elif ideal_conditions:
        datapath = cfg.dir.ideal_conditions
    elif general_encoder:
        datapath = cfg.dir.general_enc_data
    else:
        datapath = cfg.dir.data

    file_path = datapath.joinpath(f"{'' if subtree_size is None else subtree_size}_{num_events_per_edge}_time_series{'' if added_name is None else added_name}").with_suffix('.pkl')
    affected_path = datapath.joinpath(f"{'' if subtree_size is None else subtree_size}_{num_events_per_edge}_affected{'' if added_name is None else added_name}").with_suffix('.pkl')
    topo_path = datapath.joinpath(f"{'' if subtree_size is None else subtree_size}_{num_events_per_edge}_topologies{'' if added_name is None else added_name}").with_suffix('.pkl')

    with open(file_path, 'rb') as file:
        X = pickle.load(file)
    with open(topo_path, 'rb') as file:
        topologies = pickle.load(file)
    with open(affected_path, 'rb') as file:
        affected = pickle.load(file)
    return X, topologies, affected
