import torch
from tqdm import tqdm

from tools.data import get_sub_tree

import config

cfg = config.Config()
torch.random.manual_seed(7)


def parent_child_swap(A):
    cmc_children = torch.where(A[0] == 1)[0]
    internal_nodes = list(
        set(torch.where(A.sum(axis=1) > 0)[0].tolist()) - set(cmc_children.tolist()) - {0})
    if len(internal_nodes) == 0:  # If for example the whole network only has depth 2 (CMC-AMP-MODEM)
        print("Could not make a parent-child swap")
        return A

    v = internal_nodes[torch.randint(0, len(internal_nodes), (1,))]
    # Finding vs parent u
    u = torch.where(A[:, v] == 1)[0][0]
    w = torch.where(A[:, u] == 1)[0][0]
    # Reversing the direction of the relation
    A[u, v] = 0
    A[v, u] = 1
    # Making the parent of u, the parent of v instead
    A[w, u] = 0
    A[w, v] = 1
    return A


def random_swap(A, only_internal=True):
    # Perform random swap until it is not undone
    N = (A.sum(axis=1) > 0).sum()

    if only_internal:
        potential_new_child = set(range(1, N))
    else:
        potential_new_child = set(range(1, A.shape[0]))
    if A[0].sum() == 1:  # CMC has a lonely child
        lonely_child = torch.where(A[0] == 1)[0][0]
        potential_new_child = potential_new_child - {int(lonely_child)}
    if len(potential_new_child) == 0:
        print("Could not perform legal random swap")
        return A
    v = list(potential_new_child)[torch.randint(0, len(potential_new_child), (1,))]  # The new child
    # Checking if v is a leaf node
    old_parent = torch.where(A[:, v] == 1)[0][0]

    # Finding new parent
    potential = set(range(N))
    # Removing sub-tree under chosen child and the old parent
    potential = potential - set(get_sub_tree(v, A[:N, :N])) - {int(old_parent)}
    u = list(potential)[torch.randint(0, len(potential), (1, ))[0]]
    # Removing old parent and adding new
    A[old_parent, v] = 0
    A[u, v] = 1

    return A


def subtree_shuffle(A, size=None):
    """
    Sub-tree shuffle the adjacency matrix A using a sub-tree that has the size of half the internal nodes.

    """
    N = (A.sum(axis=1) > 0).sum()
    temp = A.clone()
    # Size of the subtree that should be shuffled
    size = int(torch.ceil(N / 2)) if size is None else int(size)

    # Finding the potential new roots
    potential_roots = []
    for v in range(N):
        if len(get_sub_tree(v, A[:N, :N])) >= size:
            potential_roots.append(v)
    if len(potential_roots) == 0:
        print('Could not perform sub-tree-shuffle due to size')
        return A

    # Choosing the new root
    v = potential_roots[torch.randint(0, len(potential_roots), (1,))]
    subtree = [v]
    potential_subtree = list(torch.where(A[v, :N] == 1)[0])
    for _ in range(size-1):
        idx = torch.randint(0, len(potential_subtree), (1, ))
        u = potential_subtree[idx]
        subtree.append(int(u))
        potential_subtree.pop(idx)
        potential_subtree += list(torch.where(A[u, :N] == 1)[0])
        # Removing the edge between u and its parent
        parent = torch.where(A[:, u] == 1)[0][0]
        temp[parent, u] = 0

    # Inserting new edges randomly
    while True:
        temp_adjacency = temp.clone()
        temp_sub_tree = subtree.copy()
        temp_sub_tree.pop(0)
        new_tree = [v]

        while len(temp_sub_tree) > 0:
            idx = torch.randint(0, len(temp_sub_tree), (1,))
            u = temp_sub_tree[idx]
            parent = new_tree[torch.randint(0, len(new_tree), (1, ))]
            temp_adjacency[parent, u] = 1
            temp_sub_tree.pop(idx)
            new_tree.append(u)

        if not torch.equal(A, temp_adjacency):
            break
    return temp_adjacency


def topo_random_sampler(N=4, num_samples=10, modems_per_amp=0, with_replacement=True, type="no_cycle",
                        verbose=False, check_for=None):
    """
    Randomly samples the internal part of a tree with N internal nodes whereof one (the first) is the CMC. Samples one
    vertex at random (the receiving end) and a parent from the set of vertices that would not break the assumptions
    (acyclic).

    :param N: The sub-tree size (the size of internal vertices including the CMC (root) node
    :param num_samples: The number of random topologies wanted.
    :param modems_per_amp: How many modems there should be per amplifier (0 gives you just the internal nodes)
    :param type: The type of sampling from three possibilities:
            "no_cycle": A node without a parent is chosen at random and made the child of any node that does not cause cycles.
            "already_connected": A node without a parent is chosen at random and made the child of one of the nodes already connected to the root
            "mix": Taking turns between the two starting with "no_cycle"
    :param with_replacement: If the same two topologies can be returned. If not num_samples need to be smaller than the total
    :param check_for: A tensor of topologies that can also not be sampled
    amount of permutations.
    """
    if with_replacement is False and num_samples > (N ** (N - 2)):
        raise ValueError(f"When sampling without replacement, the number of samples must not exceed the number of "
                         f"different topologies for a subtree of size {N} which is {N ** (N - 2)}. Decrease the "
                         f"number of samples or sample with replacement.")
    if type == "no_cycle":
        already_connected = False
    else:
        already_connected = True
    if check_for is None:
        check_for = torch.zeros([1, N, N])
    elif len(check_for.shape) == 2:
        check_for = check_for[None, :, :]

    if not with_replacement and num_samples > N ** (N-2):
        num_samples = N ** (N-2) // 2
        print("Number of samples adjusted because there are not that many")

    A_samples = torch.zeros((num_samples, N, N))
    for i in (tqdm(range(num_samples), desc='Sampling random topologies') if verbose else range(num_samples)):
        while A_samples[i].sum() < (N - 1):
            # Sample a node that does not have a parent
            potential_us = torch.where(A_samples[i, :, 1:].sum(axis=0) == 0)[0] + 1
            u = int(potential_us[torch.randint(0, len(potential_us), (1,))][0])

            def get_seq(j):
                # Calculating the vertices connected (downwards) to j
                if A_samples[i, j].sum() == 0:
                    return [j]
                res = [j]
                for sj in torch.where(A_samples[i, j] == 1)[0]:
                    out = get_seq(int(sj))
                    res += out if isinstance(out, list) else [out]
                return res

            if already_connected:
                potential_vs = [v for v in get_seq(0)]
                if type == "mix":
                    already_connected = False
            else:
                # Calculating which other vertices break our assumption
                # Vertices "below" u break our assumption
                potential_vs = [v for v in range(N) if v not in get_seq(u)]
                if type == "mix":
                    already_connected = True

            v = potential_vs[torch.randint(0, len(potential_vs), (1,))[0]]
            A_samples[i, v, u] = 1
            if with_replacement is False and A_samples[i].sum() == (N - 1) and ((A_samples == A_samples[i]).all(
                    dim=-1).all(dim=-1).sum() > 1 or (check_for == A_samples[i]).all(dim=-1).all(dim=-1).sum() > 0):
                A_samples[i] = 0
    # Potentially adding modems
    if modems_per_amp > 0:
        A_full = torch.zeros((N + (N - 1) * modems_per_amp, N + (N - 1) * modems_per_amp))
        for i in range(N - 1):
            A_full[i + 1, (N + i * modems_per_amp):(N + (i + 1) * modems_per_amp)] = 1
        A_full = A_full.repeat((num_samples, 1, 1))
        A_full[:, :N, :N] = A_samples
        return A_full
    return A_samples


def topo_smart_sampler(A, num_samples, replacement=False):
    """
    Smart sampler for random topologies. Output is a (num_samples + 1) x (N+M) x (N+M) array, where the first one is the true topology (A: input)
    """
    N = int((A.sum(axis=1) > 0).sum())

    if num_samples > N ** (N-2):
        num_samples = N ** (N-2) // 2
        print("Number of samples adjusted because there are not that many")

    As_random = A.repeat((num_samples + 1, 1, 1))
    swap_prob = torch.tensor(cfg.sim.swap_probabilities)
    swap_dict = {0: parent_child_swap,
                 1: random_swap,
                 2: subtree_shuffle}
    num_swaps = torch.arange(1, N + 1)
    i = 0
    found_samples = 0
    while found_samples < num_samples:
        this_A = As_random[found_samples + 1]
        this_num_swaps = num_swaps[i % N]
        swaps = torch.multinomial(swap_prob, this_num_swaps, replacement=True)
        temp = this_A.clone()
        for this_swap in swaps:
            swap_func = swap_dict[int(this_swap)]
            temp = swap_func(temp)
        if not replacement:
            if (As_random == temp[None, :, :]).all(-1).all(-1).sum() == 0:
                As_random[found_samples + 1] = temp
                found_samples += 1
        else:
            if not (As_random[0] == temp).all(-1).all(-1):
                As_random[found_samples + 1] = temp
                found_samples += 1
        i += 1
    return As_random
