import time
import torch
from tqdm import tqdm
import argparse

from tools.simulator import parsimony_uniqueness_checker, parsimony_uniqueness_smart
from tools.parser import load_permutations
from tools.sampler import topo_random_sampler
import config

cfg = config.Config()

parser = argparse.ArgumentParser()
parser.add_argument('-N', '--tree-size', default=8, type=int)

N = parser.parse_known_args()[0].tree_size
num_samples_dict = {7: 2004,
                    8: 1002,
                    9: 882,
                    10: 786,
                    15: 528}
try:
    num_samples = num_samples_dict[N]
except KeyError:
    raise ValueError('We have only tested sizes 8, 9, 10, and 15 using this method, so please use one of those')

torch.manual_seed(N)
if N > 7:
    check_against = 10000
    num_random = check_against // 2

    t1 = time.time()
    # Starting by checking the random topologies against the ones found
    As_random = topo_random_sampler(N, num_random + 1, modems_per_amp=N-1, with_replacement=False, verbose=True, type="mix").type(torch.int8)
    As_check = As_random.clone()[:num_samples]
    res, idx = parsimony_uniqueness_checker(As_random, As_check=As_check)

    unique = 0
    for i in tqdm(range(num_samples), desc="Evaluating random sampling"):
        sum_res = res[i].sum(axis=1)
        if sum_res.min() == sum_res[i]:
            if (sum_res == sum_res.min()).sum() == 1:
                unique += 1

    file_name = cfg.dir.conj_res.joinpath(f'N_{N}.txt')
    t2 = time.time()
    with open(file_name, 'w') as file:
        file.writelines([f"Tested {num_samples} tress with {N} internal nodes against {As_random.shape[0]} "
                         f"other random trees out of a total of {N**(N-2)} possible \n\n",
                         f"Ran in {t2 - t1:.2f} seconds\n\n",
                         f"Out of all {num_samples}, {unique} are the lowest and unique!\n\n\n"])
    print(f"Ran in {t2 - t1:.2f} seconds")
    print(f"Out of all {num_samples}, {unique} are the lowest and unique!")

    # %% 'Smart sampling'
    res = parsimony_uniqueness_smart(As_check, num_random=num_random, parallellize=True)

    smart_unique = 0
    for i in tqdm(range(num_samples), desc="Evaluating smart samling"):
        sum_res = res[i].sum(-1)
        if sum_res.min() == sum_res[0]:
            if (sum_res == sum_res.min()).sum() == 1:
                smart_unique += 1

    with open(file_name, 'a') as file:
        file.writelines([f"Tested {num_samples} tress with {N} internal nodes against {As_random.shape[0]} "
                         f"other smartly sampled random trees out of a total of {N**(N-2)} possible \n\n",
                         f"Ran in {time.time() - t2:.2f} seconds\n\n",
                         f"Out of all {num_samples}, {smart_unique} are the lowest and unique!\n"])
    print(f"Ran in {time.time() - t2:.2f} seconds")
    print(f"Out of all {num_samples}, {smart_unique} are the lowest and unique!")

elif N == 7:
    """
    For size 7 we want to check all the sampled topologies against all the possible ones because it is feasible
    Takes around 21.6 seconds per sample checked against all of the possible topologies
    """
    As = load_permutations(N)

    t1 = time.time()
    As_check = topo_random_sampler(N, num_samples, modems_per_amp=6, with_replacement=False, verbose=True,
                                   type="mix").type(torch.int8)
    As_new = As_check[0].clone().repeat((As.shape[0], 1, 1))
    As_new[:, :N, :N] = As
    res, idx = parsimony_uniqueness_checker(As_new, As_check=As_check)

    unique = 0
    for tt, t_idx in tqdm(zip(res, idx)):
        sum_res = tt.sum(axis=1)
        if sum_res.min() == sum_res[t_idx] and (sum_res == sum_res.min()).sum() == 1:
            unique += 1

    with open(cfg.dir.conj_res.joinpath('N_7.txt'), 'w') as file:
        file.writelines([
                            F"Tested {num_samples} tress with {N} internal nodes against {As_new.shape[0]} other trees out of a total of {N ** (N - 2)} possible \n\n",
                            f"Ran in {time.time() - t1:.2f} seconds\n\n",
                            f"Out of all {num_samples}, {unique} are the lowest and unique!\n"])
else:
    raise ValueError("Please use '1_conjecture_proof' for sizes less than 7")
