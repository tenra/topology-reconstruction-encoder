import torch.utils.data


class simulated_data(torch.utils.data.Dataset):
    """
    Pytorch wrapper for a numpy dataset
    """
    def __init__(self, time_series: list, topologies: list, affections: list):
        self.time_series = time_series
        self.topologies = topologies
        self.affections = affections

    def __len__(self):
        return len(self.time_series)

    def __getitem__(self, index):
        return self.time_series[index], self.topologies[index], self.affections[index]
