import os
import torch

from tools.simulator import simulate_general_observations
import config

cfg = config.Config()

torch.manual_seed(27)
chunk_len = 30
events_per_edge = 20
save_path = cfg.dir.general_enc_data
if not os.path.exists(save_path):
    os.mkdir(save_path)

# Sampling the smartly sampled training data
simulate_general_observations(N_min=5, N_max=20, events_per_edge=events_per_edge, chunk_len=chunk_len,
                              savepath=save_path, _type='smart', add_name='train_smart', num_samples=500,
                              parallellize=True, random_multiplier=3)

# Sampling the validation data
simulate_general_observations(N_min=5, N_max=20, events_per_edge=events_per_edge, chunk_len=chunk_len,
                              savepath=save_path, _type='mix', add_name='validation', num_samples=500,
                              parallellize=True, random_samples=500)
