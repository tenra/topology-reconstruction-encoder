# Topology Reconstruction Encoder

This repository contains the code in relation to the paper _Topology Reconstruction in Telecommunication Networks: Embedding Operations Research within Deep Learning_ which is currently under review at the journal _Computers and Operations Research_. Click [here](https://papers.ssrn.com/sol3/papers.cfm?abstract_id=4757707) for the online preprint. Make sure to cite preprint if any of the code in the repository is used or modified in any way.

This project only relates to the task of training an encoder (_f_ in the image below) able to encoder multiple time series from a customer modem into sequences of events based on the parsimony score. Hence this project does not cover the actual search for the optimal topology.

![image](plots/contrastive_approach.png)

## Getting started

### Requirements

Requirements can be installed using pip or conda from the `requirements.txt` file. `Python 3.9.7` has been used for this project. If one want to use GPUs for faster training, a version of `cuda` and a compatible GPU are also needed. This is, however, not necessary although some of the scripts will be very slow when the considered tree has more than four internal nodes.

### Sampling Data and Generating Topologies

To generate all possible topologies of a tree of a given size, run the file `all_possible_topologies.jl` in the `utility` folder using `Julia`. Remember to change the size of the network, that you want to generate, directly in the top of the script. Requirements are `CSV`, `Tables`, and `DataFrame` packages. `Julia 1.10.2` was used in this project. _Notice_: the files for topology sizes four, five, six, and seven have already been generated and given in the `utility` folder, hence it is not necessary to run this generation for the rest of the code to run.

The scipts in the `sampling`-folder will simulate the time series data for the different experiments. Notice that sampling data takes time and the resulting files are big.

## Running the Scripts

### Experiments
The different experiments can be run using the script in the `analysis` folder. Notice that if the experiments are run, the results will be concetenated onto the results already published here, hence one should change seeds or remove the old results before running the scripts. Many of the scripts should be called with the hyper-parameter setting that will be tested in a given experiment. Please check the scipts to see the list of possible parameters to call the scripts with. For many of the hyper-parameters, the default values are given in the `config.py` file in the root directory. The experiments are explained shortly below, but see the paper above for more details.

- `1_conjecture_proof.py` - Proves that the conjecture presented in the above mentioned paper holds true for trees of size up to and including six.
- `1_conjecture_sampling.py` - Performs a topology sampling experiment for a number of tree sizes greater than six. This experiment has yet to find any counter evidence for the validity of the proposed conjecture.
- `2_num_events.py` - Performs a number of repitions of experiments with a tree of a given size and a set of different numbers of events per internal edge in the tree.
- `2_num_events_6.py` - This file is the same as the above, but for trees of size six specifically, since taking all the possible topologies into account requires some small changes for computational feasibility.
- `2_multiprop.py` - Performs a number of repititions of an experiment using different values of the proportion of faults in the simulated data that occur on multiple edges.
- `2_ideal_conditions.py` - Experiments for trees of size four and five if ideal conditions are met.
- `3_sampling_experiment.py` - Performs a number of repititions of an experiment where the set of topologies considered is sampled using two different approaches rather than simply considering every possible topology.
- `4_general_enc.py` - Performs a number of repititions of an experiment where the size of the trees in the data can vary in size between four and twenty.

### Plots and Results

The plots and final results are generated using the `make_plots.py` files in the `analysis` folder based on the results given in the `results` folder. These results are based on the experiment scripts given above.
