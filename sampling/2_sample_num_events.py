import os
import torch
import argparse

from tools.simulator import simulate_observations
import config

cfg = config.Config()

parser = argparse.ArgumentParser()
# Training parameters
parser.add_argument('-N', '--subtree-size', default=6, type=int)
args = parser.parse_known_args()[0]

N = args.subtree_size
torch.manual_seed(N)

chunk_len = 30
save_path = cfg.dir.num_events_data
if not os.path.exists(save_path):
    os.mkdir(save_path)

if N == 4:
    num_event_per_edge_list = [2, 4, 6, 8, 10, 12, 14, 16, 18, 20]
elif N == 5:
    num_event_per_edge_list = [2, 4, 6, 8, 10, 12, 14, 16]
else:
    num_event_per_edge_list = [2, 4, 6, 8, 10]

for events_per_edge in num_event_per_edge_list:
    simulate_observations(N, events_per_edge=events_per_edge, chunk_len=chunk_len, savepath=save_path)
