import numpy as np
import torch
from tools.data import get_seq
from torch.nn.modules.loss import _Loss


def parsimony(D, A, N: int = None, sum_over_time: bool = True,
              device=torch.device('cpu'), binary: bool = True,
              normalize: bool = True):
    """
    Calculates the parsimony score using a set of events, the adjacency matrix, and optionally the number of internal nodes

    :param device: Which device the operations are to computed using
    :param sum_over_time: If the parsimony score should be summed over time
    :param D: The event matrix - should have shape (M x T)
    :param A: Adjacency matrix - should have shape ((M+N) x (M+N))
    :param N: The number of internal nodes (amplifiers/splitters and the CMC) defaults to the number of nodes with children
    :param binary: If the input is binary and all intermediate steps should also be
    :param normalize: If the cost should be divided by the number of children (or not)
    :return: The parsimony score (minimum number of mutations taking place over modems and time)
    """
    D = torch.round(D) if binary else D
    if N is None:
        N = torch.sum(A.sum(axis=1) > 0)
    D = torch.stack((-D + 1.0, D), -1)
    D = torch.cat(((torch.empty((N, D.shape[1], D.shape[2])) * torch.nan).to(device), D), 0)

    cost = torch.zeros((N, D.shape[1])).to(device)
    for i in get_seq(0, A[:N, :N]):
        children = torch.where(A[i] == 1)[0]
        num_children = len(children)
        if num_children == 0:  # Amplifier is a leaf node (can happen for some random permutations)
            D[i] = torch.zeros(D.shape[1:])
            mx = torch.zeros(D.shape[1])
        else:
            d_children = D[children].sum(axis=0)  # Summing the occurrences of each letter (allowing same number)
            mx = d_children.max(axis=1).values
            D[i] = torch.eq(d_children, mx.unsqueeze(-1)) if binary else d_children / num_children
        # Here we sum up the mutations over time (or characters) instead of doing the maximum over characters
        cost[i, :] = num_children - mx if i != 0 else d_children[:, 1]
        if normalize:
            cost[i, :] /= num_children

    if sum_over_time:
        return cost.sum()
    return cost.sum(dim=0)


class ParsimonyLoss(_Loss):
    def __init__(self, negative_weight=1.0, n_random_samples=5, subtree=False,
                 binary=True, device=torch.device('cpu'), normalize=True):
        super(ParsimonyLoss, self).__init__()
        self.n_random_samples = n_random_samples
        self.negative_weight = negative_weight
        self.subtree = subtree
        self.device = device
        self.binary = binary
        self.normalize = normalize

    def forward(self, D, A, A_neg):
        """
        Calculates the loss given an input D, a true topology matrix A, and a selection of negative topologies given in A_neg

        :param D: An M x (T * D) matrix where M is the number of modems, T is the length in time, and D is the number of output variables
        :param A: An M x M matrix specifying the topology of the true network
        :param A_neg: An NN
        :return:
        """
        N = torch.sum(A.sum(axis=1) > 0)
        # Concatenating the different variables to a single vector in time per modem
        D = D.reshape((D.shape[0], -1))
        T = D.shape[1]
        # Positive parsimony
        pars_pos = parsimony(D, A, N, binary=self.binary, sum_over_time=False, device=self.device,
                             normalize=self.normalize)

        # Negative parsimony pulled randomly
        if self.subtree:
            pars_neg = torch.zeros((A_neg.shape[0] + 1, T)).to(self.device)
            for i, j in enumerate(range(A_neg.shape[0])):
                pars_neg[i + 1] = parsimony(D, A_neg[j], N, binary=self.binary, sum_over_time=False,
                                            normalize=self.normalize, device=self.device)
        else:
            pars_neg = torch.zeros((self.n_random_samples + 1, T)).to(self.device)
            negative_samples = np.random.choice(A_neg.shape[0], self.n_random_samples, replace=False)
            for i, j in enumerate(negative_samples):
                pars_neg[i + 1] = parsimony(D, A_neg[j], N, binary=self.binary, sum_over_time=False,
                                            normalize=self.normalize, device=self.device)
        pars_neg[0] = pars_pos

        distance_loss = torch.sqrt(torch.sum(
            1 / (torch.cdist(pars_neg, pars_neg, compute_mode='donot_use_mm_for_euclid_dist').pow(2) + 1)) /
                                   (pars_neg.shape[0] * (pars_neg.shape[0] - 1)))

        parsimony_loss = torch.sum(pars_pos) * 2 / (A.shape[0] * T)

        return (parsimony_loss + self.negative_weight * distance_loss,
                parsimony_loss,
                self.negative_weight * distance_loss)
