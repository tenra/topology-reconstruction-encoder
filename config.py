from pathlib import Path


class Config(object):
    def __init__(self):

        # The different sub-classes
        self.dir = Dir()
        self.model = ModelConfig()

        # Simulating coefficients
        self.sim = SimulationParams()

        self.num_epochs = 10
        self.learning_rate = 0.01
        self.weight_decay = 0.001
        self.num_batches = 54  # 35  # 27
        self.random_samples = 5
        self.negative_weight = 30.0


class ModelConfig(object):
    def __init__(self):
        self.in_channels = 1
        self.depth = 3
        self.kernel_size = 5
        self.channels = 16
        self.reduced_size = 8
        self.out_channels = 1


class SimulationParams(object):
    def __init__(self):
        self.num_samples = 1000
        self.num_events_per_amp = 20
        self.min_modems_per_amp = 2
        self.no_event_prob = 0.5
        self.N = 4
        self.D = 1
        self.T = 1500
        self.mu = 0.0
        self.sigma = 0.5
        self.phi_1 = 0.6
        self.phi_2 = -0.5
        self.deviations = [3.0]
        self.swap_probabilities = [0.45, 0.45, 0.1]  # Probabilities for the [parent-child swap, random swap, subtree shuffle]


class Dir(object):
    def __init__(self):
        self.root = Path(__file__).absolute().parent

        self.analysis = self.root.joinpath("analysis")

        # Data directories
        self.data = self.root.joinpath('data')
        # Should be in big_data
        self.multi_prop_data = self.data.joinpath('multi_prop')
        self.num_events_data = self.data.joinpath('num_events')
        self.samp_strat_data = self.data.joinpath('sampling_strategy')
        self.general_enc_data = self.data.joinpath('general_enc')
        self.ideal_conditions = self.data.joinpath('ideal_conditions')

        self.subtree = self.data.joinpath('subtree')
        self.results = self.root.joinpath('results')
        self.plots = self.root.joinpath('plots')

        self.conj_res = self.results.joinpath('conjecture')
