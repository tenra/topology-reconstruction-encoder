import os
import csv
import argparse

import torch
from tqdm import tqdm

from models.causal_cnn import CNNEncoder
from utility.data import simulated_data
from utility.loss import ParsimonyLoss, parsimony
from tools.parser import load_saved_data
import config

cfg = config.Config()

parser = argparse.ArgumentParser()
# Training parameters
parser.add_argument('-lr', '--learning-rate', default=cfg.learning_rate, type=float)
parser.add_argument('-wd', '--weight-decay', default=cfg.weight_decay, type=float)
parser.add_argument('-mn', '--model-name', default='best_model')
parser.add_argument('-rs', '--random-samples', default=cfg.random_samples, type=int)
parser.add_argument('-np', '--negative-weight', default=cfg.negative_weight, type=float)
parser.add_argument('-nb', '--num-batches', default=cfg.num_batches, type=int)
parser.add_argument('-ed', '--euclidean-dist', default=False, type=bool)
parser.add_argument('-gc', '--gradient-clipping', default=None, type=float)
parser.add_argument('-ad', '--addition-const', default=1.0, type=float)
parser.add_argument('-ne', '--num-epochs', default=1, type=int)
parser.add_argument('-nr', '--num-repetitions', default=10, type=int)
parser.add_argument('-gn', '--gpu-num', default=None, type=int)
parser.add_argument('-SS', '--sampling-strategy', default='smart', type=str)
# Model parameters
parser.add_argument('-cau', '--causal', default=False, type=bool)
parser.add_argument('-d', '--depth', default=cfg.model.depth, type=int)
parser.add_argument('-k', '--kernel', default=cfg.model.kernel_size, type=int)
parser.add_argument('-c', '--channels', default=cfg.model.channels, type=int)
parser.add_argument('-r', '--reduced-size', default=cfg.model.reduced_size, type=int)
parser.add_argument('-oc', '--out-channels', default=cfg.model.out_channels, type=int)
parser.add_argument('-N', '--subtree-size', default=cfg.sim.N, type=int)

args = parser.parse_known_args()[0]
N = args.subtree_size

torch.manual_seed(N)
if torch.cuda.is_available():
    print("Using CUDA")
    device = torch.device(f'cuda:{args.gpu_num}') if args.gpu_num is not None else torch.device('cuda')
else:
    print("Using CPU")
    device = torch.device('cpu')

# Create a data file if it does not already exist
result_file = cfg.dir.results.joinpath(f'sampling_strat/{args.sampling_strategy}_{N}.csv')

if not os.path.exists(result_file):
    with open(result_file, 'w') as file:
        csv.writer(file).writerow(['rep', 'num_samples', 'alpha', 'topo_acc', 'topo_unique_acc',
                                   'event_acc_mean', 'event_acc_std', 'prop_ones'])
        file.close()

X_train, topo_train, affected_train = load_saved_data(N, sampling=True,
                                                      added_name=f'_train_{args.sampling_strategy}')
X_test, topo_test, affected_test = load_saved_data(N, sampling=True,
                                                   added_name=f'_validation')
train_loader = simulated_data(X_train, topo_train, affected_train)
del X_train, topo_train, affected_train
test_loader = simulated_data(X_test, topo_test, affected_test)
del X_test, topo_test, affected_test

investigate_samples = torch.cumsum(torch.round(torch.exp(torch.arange(10) * torch.log(torch.tensor([N])) / 10)), dim=0)
investigate_samples = torch.concat((investigate_samples[investigate_samples < 3 * N], torch.tensor([3 * N])))
for num_samples in investigate_samples:
    # %% Defining the network and the learning functionality
    for i in range(args.num_repetitions):
        print(
            f"{f' Doing run {i + 1} of {args.num_repetitions} evaluating {int(num_samples)} {args.sampling_strategy} samples N = {N}':-^100}")

        model_config = dict(in_channels=cfg.sim.D,
                            depth=args.depth,
                            kernel_size=args.kernel,
                            channels=args.channels,
                            reduced_size=args.reduced_size,
                            out_channels=args.out_channels,
                            causal=args.causal,
                            dropout=0.3)
        model = CNNEncoder(**model_config)
        model.to(device)

        loss_config = dict(negative_weight=args.negative_weight,
                           subtree=True,
                           binary=False,
                           device=device,
                           normalize=False)
        loss_func = ParsimonyLoss(**loss_config)

        loss_config['binary'] = True
        bin_loss_func = ParsimonyLoss(**loss_config)

        optimizer = torch.optim.Adam(model.parameters(), weight_decay=args.weight_decay, lr=args.learning_rate)

        # %% Training loop (one epoch is sufficient)
        for epoch in range(1):
            print(f"{f' Epoch {epoch + 1} of {1} ':-^100s}")

            model.train()
            train_bar = tqdm(train_loader, total=len(train_loader), ncols=100)

            running_train = None
            batch_i = 0
            optimizer.zero_grad()
            for x, topo, affected in train_bar:
                batch_i += 1
                pred = model(x.to(device))

                train_loss, train_p_loss, train_d_loss = loss_func(pred, topo[0].to(device),
                                                                   topo[1:int(num_samples + 1)].to(device))
                # Calculating the binary version
                with torch.no_grad():
                    bin_loss, bin_pars, bin_dist = bin_loss_func(torch.round(pred), topo[0].to(device),
                                                                 topo[1:int(num_samples + 1)].to(device))

                optimizer.zero_grad()
                if batch_i == 1:
                    train_loss.backward()
                else:
                    train_loss.backward(retain_graph=True)

                if batch_i == 5:
                    optimizer.step()
                    optimizer.zero_grad()
                    batch_i = 0

                if running_train is None:
                    running_train = train_loss.detach().cpu().numpy()
                else:
                    running_train = 0.9 * running_train + 0.1 * train_loss.detach().cpu().numpy()
                if torch.isnan(torch.tensor(running_train)):
                    print('')
                train_bar.set_description(f"Training loss: {running_train:.4f}")

        model.eval()
        # Should the bits be flipped?
        flip = 0
        for x, topo, affected in tqdm(train_loader, desc='Flipping bits?'):
            with torch.no_grad():
                pred = model(x.to(device))

                _, pars, _ = loss_func(pred, topo[0].to(device), topo[1:int(num_samples + 1)].to(device))
                _, pars2, _ = loss_func((pred * 1.0 - 1.0) * (-1.0), topo[0].to(device),
                                        topo[1:int(num_samples + 1)].to(device))

                if pars2 < pars:
                    flip += 1

        flip_bits = flip / len(train_loader) > 0.5

        # Evaluating according to the validation set
        val_bar = tqdm(test_loader, total=len(test_loader), ncols=100, desc='Validating topologies')

        running_eval = None
        min_, min_unique = 0, 0
        accuracies, _prop_ones, all_parsimonies = [], [], []
        for j, (x, topo, affected) in enumerate(val_bar):
            with torch.no_grad():
                pred = model(x.to(device))

                if flip_bits:
                    pred = (pred * 1.0 - 1.0) * (-1.0)
                pred = torch.round(pred)
                _prop_ones.append(torch.mean(pred))

                # Evaluating the event accuracy
                accuracies.append(((pred == affected.to(device)) * 1.0).mean())

                # Evaluating the binary version for all different topologies
                parsimonies = []
                for t in topo:
                    parsimonies.append(
                        parsimony(pred[:, 0, :], t, sum_over_time=True, device=device, normalize=False))

                parsimonies = torch.stack(parsimonies)
                if parsimonies[0] == torch.min(parsimonies):
                    min_ += 1
                    if (parsimonies == parsimonies[0]).sum() == 1:
                        min_unique += 1
                all_parsimonies.append(parsimonies)

        result_path = cfg.dir.results.joinpath(f'sampling_strat/parsimony_results/{N}_parsimonies_strat_{args.sampling_strategy}_numsamples_{num_samples}_run_{i}').with_suffix('.pt')
        torch.save(torch.stack(all_parsimonies).detach().cpu(), result_path)
        model_path = cfg.dir.root.joinpath(f'models/saved_models/{N}_model_strat_{args.sampling_strategy}_numsamples_{num_samples}_run_{i}').with_suffix('.pkl')
        torch.save(model.state_dict(), model_path)

        # Saving the results
        topo_acc = min_ / len(test_loader)
        topo_unique_acc = min_unique / len(test_loader)
        accuracies = torch.stack(accuracies)
        prop_ones = torch.stack(_prop_ones).mean().detach().cpu().numpy()
        event_acc_mean = torch.mean(accuracies).detach().cpu().numpy()
        event_acc_std = torch.std(accuracies).detach().cpu().numpy()

        # Write a row in the result file
        with open(result_file, 'a') as file:
            csv.writer(file).writerow([i,
                                       int(num_samples),
                                       args.negative_weight,
                                       topo_acc,
                                       topo_unique_acc,
                                       event_acc_mean,
                                       event_acc_std,
                                       prop_ones])
            file.close()
