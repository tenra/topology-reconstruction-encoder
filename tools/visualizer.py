import graphviz
import config

cfg = config.Config()


def plot_graph(A, savePath=None, layout="dot", view=True, labels=False, only_modems=False):
    """
    Plots the graph of the network specified by the adjacency matrix, A.

    :param A: Adjacency matrix - the first element N elements are the internal nodes and the first element is the CMC
    :param savePath: The path to where the figure should be saved to
    :param layout: Which layout should be used to draw the graph
    :param view: If the plot should be immediately shown
    :param labels: If the edges should be given labels according to the number of the child
    :param only_modems: If only the labels of the modems should be plotted
    :return: A graph figure
    """
    if savePath is None and view is False:
        raise ValueError("You either have to see the plot or save it!")
    if len(A.shape) != 2:
        raise ValueError(f"Input adjacency matrix needs to have 2 dimensions. You gave {len(A.shape)}")
    # Calculating the number of internal edges from the number of nodes that have children
    N = (A.sum(axis=1) > 0).sum()

    tree = graphviz.Graph(engine=layout,
                          graph_attr={'concentrate': 'True',
                                      'outputorder': 'edgesfirst'},
                          node_attr={'color': 'red', 'shape': 'point'},
                          edge_attr={'color': 'black', 'penwidth': '.5'})

    # Adding the nodes to the tree
    tree.node('0', color="green")
    for amp in range(1, N):
        tree.node(f'{amp}', color="red")
    for modem in range(N, A.shape[0]):
        tree.node(f'{modem}', color='blue')

    # Plotting the edges
    for _from in range(A.shape[0]):
        for _to in range(A.shape[0]):
            if A[_from, _to] > 0:
                if labels:
                    if only_modems is False:
                        tree.edge(f'{_from}', f'{_to}', label=f"{_to}")
                    elif _to >= N:
                        tree.edge(f'{_from}', f'{_to}', label=f"{_to-N}")
                    else:
                        tree.edge(f'{_from}', f'{_to}')
                else:
                    tree.edge(f'{_from}', f'{_to}')

    if savePath is None:
        savePath = cfg.dir.root.joinpath('temp')
    tree.render(str(savePath), format='png', view=view)
