import os
import torch
import argparse

from tools.simulator import simulate_observations
import config

cfg = config.Config()

parser = argparse.ArgumentParser()
# Training parameters
parser.add_argument('-N', '--subtree-size', default=4, type=int)
args = parser.parse_known_args()[0]

N = args.subtree_size
torch.manual_seed(N)
chunk_len = 50
save_path = cfg.dir.ideal_conditions
if not os.path.exists(save_path):
    os.mkdir(save_path)

for events_per_edge in [20]:
    simulate_observations(N, events_per_edge=events_per_edge, chunk_len=chunk_len, savepath=save_path)
