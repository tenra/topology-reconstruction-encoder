using CSV
using Tables
using DataFrames
#=__________________________________________________________________________
                            GLOBAL VARIABLES.
                           EDIT TO YOUR NEEDS!
                          Refrain from editing
                       other sections than this one
__________________________________________________________________________=#
path_sol = pwd()*"/"    # path so save solution files set to the parent directory of this file. Can be changed.

println("\nPlease enter the size of the network (Int):")
first_input = readline(stdin)
first_value = tryparse(Int, first_input)
if first_value !== nothing
	SUBTREE_SIZE = first_value
else
	@warn "Enter a whole number between 2 and 7. Try again from the beginning."
end

#=__________________________________________________________________________
                                   Warning
__________________________________________________________________________=#

if SUBTREE_SIZE > 7
  while true
    println("\nWARNING! You have selected a tree size of ",SUBTREE_SIZE,
    ", which will generate ", SUBTREE_SIZE^(SUBTREE_SIZE-2), " topologies.\n
    A tree size > 7 is not recommended due to computational time.\n
    Do you wish to continue (y/n)? Or edit the tree size (e)?\n")
    input = readline(stdin)
    value = input
    if value !== nothing && (value == "y" || value == "n" || value == "e" )
        if value == "y"
          println("Don't say I didn't warn you!")
          break
        elseif value == "n"
          println("\nSmart choice!")
          exit()
        else
          println("Please enter a new tree size (Int):")
          input = readline(stdin)
          value = tryparse(Int, input)
          if value !== nothing && 2 <= value <= 7
            println("Redefining tree size to: $(input)")
            global SUBTREE_SIZE = value
            break
          else
            @warn "Enter a whole number between 2 and 7. Try again from the beginning."
          end
        end
    else
        @warn "Enter valid input: y/n/e"
    end
  end
end

#=__________________________________________________________________________
                      Generate all possible topologies
__________________________________________________________________________=#
function gen_top()
  #=
  With a graph of n nodes, there are n^(n-2) possible topologies.
  With n=4 there are 16 topologies, for n=5 there are 125 and 1296
  for n=6:
  =#
  num_top = SUBTREE_SIZE^(SUBTREE_SIZE-2)

  # data matrix M. Each line corresponds to a topology.
  # each row contains n*(n-1)/2 entries corresponding to the upper
  # right triangle of the adjacency matrix stacked. Example:
  #  0 a b c
  #  0 0 d e
  #  0 0 0 f
  #  0 0 0 0
  # becomes row: 'a b c d e f' in data matrix M of length=6 (r_len)
  r_len = Int.((SUBTREE_SIZE)*(SUBTREE_SIZE-1)/2)
  M = zeros(Int64, num_top, r_len)

  # start and end index for each 'matrix row' in one topology entry of M:
  # e.g. for n = 4, we have:
  # row_s_e = [1 3; 4,5; 6,6]
  row_s_e = zeros(Int64, SUBTREE_SIZE-1, 2)
  j = 0
  for k in 1:(SUBTREE_SIZE-1)
    i = j+1
    j = i + (SUBTREE_SIZE-1-k)
    row_s_e[k,:] = [i j]
  end
  #println(row_s_e)

  # position for each n-1 edge we need to choose to get a tree:
  pos = collect( 1:(SUBTREE_SIZE-1) )     # i.e. p = [1, 2, 3, ..., n-1]

  row = 1
  while row <= num_top       # while we haven't filled out all rows of M
    # add the topology based on position of each edge:
    for col in pos
      M[row, col] = 1
    end

    # check that no node is 'alone':
    cycle_or_disconnected = false
    #=  MIGTH ADD SPEED UP BUT DOES NOTHING FOR CORRECTNESS TO LEAVE OUT:
    #________________________________________________________________________
    for section in 2:(SUBTREE_SIZE-1)       #section is each upper triangle row
      id = row_s_e[section,:]               #extract start and end id of section
      # corresponding row and colum sums must not both be zero (cycle or disconnected)
      if isequal( sum( M[ row, id[1]:id[2] ] ), 0 ) && isequal( sum(M[ row, row_s_e[1:(section-1), 2] .- (SUBTREE_SIZE - section) ]), 0 )
        cycle_or_disconnected = true; #println("Node is alone!!");
      end
    end
    #check first row and last column seperately, they must also not be zero (disconnected)
    if isequal( sum(M[row, row_s_e[:,2]]), 0 ) || isequal( sum(M[row, row_s_e[:, 2]]), 0 )
      cycle_or_disconnected = true; #println("Node is alone!!");
    #________________________________________________________________________
    end
    =#


    #DFS to find cycle or disconnected:
    # start in node 1. Add children, then children of children. Two condition must be meet:
    # 1) We must find all nodes.
    # 2) we must never
    if !cycle_or_disconnected           # if so far so good, check for cycle and unconnectivity:
      visited = zeros(Int, SUBTREE_SIZE)      #nodes we found in search, (1) if found
      list = zeros(Int, SUBTREE_SIZE + 1 )    # will at most process each node once. +1 to not go out of bounce
      list_p = zeros(Int, SUBTREE_SIZE + 1)   # list of parent node of node in list

      # adjacency lists for each node (undirected)
      adj_list = [ [] for i = 1:SUBTREE_SIZE ]
      id_list = 1
      for i = 1:SUBTREE_SIZE-1
        for j = i+1:SUBTREE_SIZE
          if isequal(M[row, id_list], 1)
            push!( adj_list[i], j)
            push!( adj_list[j], i)
          end
          id_list += 1;
        end
      end

      # check no adj list is empty if deleting code for check 'alone' node

      #init:
      list[1] = 1

      visited[1] = 1
      id_curr = 1
      id_next = 2
      parent = -1

      # check node at a time starting in 1. Mark connected nodes as visited and add to list.
      # process list untill reaching a zero (cannot reach all nodes from 1) or untill visit
      # node twice.
      while !isequal( list[id_curr], 0) && !cycle_or_disconnected     #reach end of list or cycle
        for node in adj_list[ list[id_curr] ]                              # loop over connected nodes
          if isequal( visited[node], 1) && node != list_p[ id_curr ]  # has visited before and is not parent
            cycle_or_disconnected = true
          elseif !isequal(node, list_p[ id_curr ])      # new node is not parent -> add to list
            visited[node] = 1
            list_p[ id_next ] = list[id_curr]
            list[ id_next ] = node
            id_next += 1
          end
        end
        id_curr += 1
      end

      if !(id_curr > SUBTREE_SIZE)
        cycle_or_disconnected = true
      end

    end

    # if cycle/discon overwrite:
    if cycle_or_disconnected
      #println("Found topology with cycle, skipped!")
      for col in pos
        M[row, col] = 0             # overwriting (cycle) solution
      end
    else
      row += 1                      #ready to find next topo
    end

    # Find pos of next topology:
    if row <= num_top               #cannot find next topology if there are no more to find
      # Find out how many section pointers to update. Start at last section and work
      # your way through the sections last to first.
      # Once the 'earliest/first section that needs updating has been found, it will
      # be updated as well as all sections to its RIGHT.
      continuee = true
      section = (SUBTREE_SIZE-1)                            # last section is starting point
      first_to_update = 0

      while continuee
        first_to_update = section
        if isequal( pos[section], row_s_e[section, 2] )    #if pos is at section end, prev section must also be updated
          section -= 1
        else
          continuee = false
        end
      end

      # 'first section to update' must be moved +1 right. Remainder sections
      # must be moved to the postions following 'first-moved-section's position'
      pos[first_to_update] += 1
      if pos[first_to_update] > row_s_e[first_to_update, 2]; error("Section pos too far right!"); end
      for section in (first_to_update + 1):(SUBTREE_SIZE - 1)
        pos[section] = pos[section-1] + 1
        if pos[section] > row_s_e[section, 2]; error("Section pos too far right!"); end
      end
    end
  end
  return M
end

#=__________________________________________________________________________
                          save solution as .csv
__________________________________________________________________________=#
function save_solution(M::Matrix{Int64})
  #name of .csv file:
  out_name = path_sol*"permutations_"*string(SUBTREE_SIZE)*".csv"

  #write 'all-possible-topologies' data matrix based on num of splitters.
  CSV.write(out_name,  Tables.table(M), writeheader=false, append=false)

  println("Solution saved to location:\n",out_name)
end

#=__________________________________________________________________________
                                  main
__________________________________________________________________________=#
#generate all possible topologies
println("--- Generating all possible tree topologies with ",SUBTREE_SIZE," internal nodes ---\n")
M = gen_top()

#save solution file
save_solution(M)
