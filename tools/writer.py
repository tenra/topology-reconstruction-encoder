import pickle
from pathlib import PosixPath, Path


def save_pickle(path, obj, verbose=True):
    """
    Pickles the given object at the given location

    :param verbose: Printing or not
    :param path: path to where the object should be pickled
    :param obj: the object to be pickled
    """
    path = Path(path).with_suffix('.pkl')
    file_handler = open(path, 'wb')
    pickle.dump(obj, file_handler)
    file_handler.close()
    if verbose:
        print(f"{f' Pickled object at {path} ':-^{150}}")
