import torch


def get_seq(v, A):
    """
    (Recurrent function)
    Sequences the vertices (or all points) so that any index that comes before another, is always below it in the tree
    given in the adjacency matrix.
    Can also be used to return the nodes in the sub-tree given with v as the root node.

    :param v: index to search from (0 for the root node)
    :param A: Adjacency matrix (can use only the part corresponding to amplifiers A[:N, :N])
    :return: A list of indices in the sub-tree given with v as the root node.
    """
    children = torch.where(A[v] == 1)[0]
    if len(children) == 0:
        return [v]
    return [int(a) for ch in children for a in get_seq(ch, A)] + [v]


def get_child_modems(v, A):
    """
    For a given node and adjacency matrix, return a list of all leaf nodes under v. Returns itself
    if it is itself a lead node.

    :param A: Adjacency matrix (non-symmetric, i.e. tree)
    :param v: the index of the node
    :return:
    """
    if  not isinstance(A, torch.Tensor):
        A = torch.from_numpy(A)
    children = torch.where(A[v] == 1)[0]
    if len(children) == 0:
        return [v]
    else:
        return [int(c) for u in children for c in get_child_modems(u, A)]


def get_sub_tree(v, A, depth=None):
    """
    Returns the nodes in the subtree given with v as the root node of a maximum depth

    :param v: root node in tree
    :param A: adjacency matrix (only give the amplifier-part of internal nodes)
    :param depth: integer
    :return: list of indices of the modems beneath v, but only depth away
    """
    if depth is None:
        return get_seq(v, A)
    all = [v]
    level = [v]
    for i in range(depth):
        next_level = []
        for ch in level:
            children = torch.where(A[ch] == 1)[0]
            if len(children) > 0:
                next_level += [int(child) for child in children]
        all += next_level
        level = next_level

    return all


def upper_vector_to_full(vector):
    """
    Takes as input a vector of the values of the upper triangles in the given adjacency matrix, assuming that it is symmetric and with
    zeros in the diagonal. Returns the full adjacency matrix for the tree rooted in the node with index 0 and directed from the root
    and downwards.

    :param vector: A vector of the upper triangle adjacencies
    :return: The full N x N adjacency matrix rooted in the node with index 0 and directed from the root and down
    """
    n = int((1 + (1 + 8 * len(vector)) ** 0.5) / 2)
    full_matrix = torch.zeros(n, n).long()
    # We dont have the diagonal
    upper_indices = torch.triu_indices(n - 1, n - 1)
    full_matrix[upper_indices[0], upper_indices[1] + 1] = vector
    full_matrix = full_matrix + full_matrix.T  # Making the undirected matrix
    # Removing some of the edges to make it directed
    full_matrix[:, 0] = 0
    full_matrix[1:, torch.where(full_matrix[0] == 1)[0]] = 0
    # Removing the rest based on presence elsewhere
    too_many = torch.where(full_matrix.sum(dim=0) > 1)[0]
    while len(too_many) > 0:
        for j in too_many:
            i_s = torch.where(full_matrix[:, j] == 1)[0]
            for i in i_s:  # They dont have a brother (the other direction)
                if full_matrix[j, i] == 0:
                    full_matrix[i_s, j] = 0
                    full_matrix[i, j] = 1
                    break
        too_many = torch.where(full_matrix.sum(dim=0) > 1)[0]
    return full_matrix
