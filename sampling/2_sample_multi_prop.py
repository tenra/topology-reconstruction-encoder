import os
import argparse

import torch
from tools.simulator import simulate_observations
import config

cfg = config.Config()

parser = argparse.ArgumentParser()
parser.add_argument('-N', '--subtree-size', default=cfg.sim.N, type=int)
N = parser.parse_known_args()[0].subtree_size

save_path = cfg.dir.multi_prop_data
if not os.path.exists(save_path):
    os.mkdir(save_path)

torch.manual_seed(N)
for multi_prop in torch.arange(11) / 10:
    simulate_observations(N=N, prop_multi=multi_prop, savepath=save_path, chunk_len=50,
                          events_per_edge=20, add_name=f'multiprop_{int(multi_prop * 100)}')
