from tools.parser import load_permutations
from tools.data import get_child_modems
from tools.writer import save_pickle
from utility.loss import parsimony
from tools.sampler import topo_smart_sampler, topo_random_sampler
import config

import torch
from tqdm import tqdm
from joblib import Parallel, delayed

cfg = config.Config()


def base_signal(D: int = 5, T: int = 1000, phi_1=0.6, phi_2=-0.5, y0s=None, mu_t=None, sigma_t=None):
    if mu_t is None:
        mu_t = torch.zeros((D, T))
    else:
        mu_t = torch.tensor([mu_t]) if isinstance(mu_t, float) or isinstance(mu_t, int) else \
            (mu_t if isinstance(mu_t, torch.Tensor) else torch.tensor(mu_t))
        if len(mu_t.shape) < 2:
            if len(mu_t) == D:
                mu_t = torch.ones((D, T)) * mu_t[:, None]
            elif len(mu_t) == 1:
                mu_t = torch.ones((D, T)) * mu_t
            else:
                mu_t = torch.ones((D, T)) * mu_t[None, :]
    if sigma_t is None:
        sigma_t = torch.ones((D, T))
    else:
        sigma_t = torch.tensor([sigma_t]) if isinstance(sigma_t, float) or isinstance(sigma_t, int) else (
            torch.tensor(sigma_t))
        if len(sigma_t.shape) < 2:
            if len(sigma_t) == D:
                sigma_t = torch.ones((D, T)) * sigma_t[:, None]
            elif len(sigma_t) == 1:
                sigma_t = torch.ones((D, T)) * sigma_t
            else:
                sigma_t = torch.ones((D, T)) * sigma_t[None, :]

    y0s = mu_t[:, 0] if y0s is None else y0s
    signal = torch.zeros((D, T))
    signal[:, 0] = signal[:, 1] = y0s
    for t in range(2, T):
        signal[:, t] = (phi_1 * signal[:, t - 1] + phi_2 * signal[:, t - 2] +
                        torch.randn((D,)) * sigma_t[:, t] + mu_t[:, t])
    return signal


def simulate_observations(N=None, events_per_edge=None, no_event_prop=None, prop_multi=0, savepath=None, add_name=None,
                          chunk_len=None, num_samples=None, type='full', num_random=500, parallellize=True,
                          max_modems=None):
    """
    Single tree size observation simulator.
    The simulating function that simulates time series and adds events according to the number of events per amp at
    random. The time series is chopped into a number of blocks where each block either has an event (multifault at rate
    prop_multi) or not.
    The type is the type of sampling. 'full' gives the full topology set. If name is either 'random' or 'smart',
    num_random observations will be generated. 'random' is random, while 'smart' is based on the true topology and
    sampling from it.
    """
    no_event_prop = cfg.sim.no_event_prob if no_event_prop is None else no_event_prop
    num_samples = cfg.sim.num_samples if num_samples is None else num_samples
    events_per_edge = cfg.sim.num_events_per_amp if events_per_edge is None else events_per_edge
    N = cfg.sim.N if N is None else N
    E = N - 1
    D = cfg.sim.D
    max_modems = (N - 1) * 10 if max_modems is None else max_modems
    # For a constant chunk length, T will be defined from the N and events_per_amp
    if chunk_len is None:
        T = cfg.sim.T
    else:
        T = int(E * events_per_edge * chunk_len)

    if num_random > (N ** (N - 2)):
        num_random = (N ** (N - 2)) - 1

    if type == 'full':
        As = load_permutations(N)
        num_permutations = As.shape[0]
    else:
        As = topo_random_sampler(N, num_samples=num_samples, type='mix')
        num_permutations = num_random + 1
    observations, topologies, affections = [], [], []

    # for _ in tqdm(range(num_samples), desc='Simulating observations'):
    print(f"{f' Simulating {T} observations for trees of size {N} with {events_per_edge} mean events pr edge ':-^100s}")

    def simulate(num):
        if num % 10 == 0:
            print(f"{num} of {num_samples}")
        # Simulate base signal and a number of modem children
        M = torch.randint((N - 1) * 5, max_modems, (1,))[0]  # Between 5 and 10 modems per amp

        # Generating the Adjacency matrix and sampling a random permutation
        A = torch.zeros((M + N, M + N))
        if type == 'full':
            this_topo = torch.randint(0, num_permutations, (1,))[0]
            A[:N, :N] = As[this_topo]
        else:
            A[:N, :N] = As[num]
        # We want a minimum of 2 modems per amplifier
        num_per_amp = torch.concat((torch.tensor([0]),
                                    torch.cumsum(
                                        torch.bincount(
                                            torch.randint(0, N - 1, (M - (N - 1) * cfg.sim.min_modems_per_amp,)),
                                            minlength=N - 1) + cfg.sim.min_modems_per_amp, 0))) + N
        for j in range(N - 1):
            A[j + 1, num_per_amp[j]:num_per_amp[j + 1]] = 1
        # Generating the full topology tensor
        full_topo = A.repeat((num_permutations, 1, 1))
        if type == 'full':
            full_topo[1:, :N, :N] = As[[x for x in range(num_permutations) if x != this_topo]]
        elif type == 'smart':
            full_topo[:, :N, :N] = topo_smart_sampler(A, num_random)[:, :N, :N]
        elif type == 'mix':
            halfway = num_permutations // 2 + num_permutations % 2
            full_topo[:halfway, :N, :N] = topo_smart_sampler(A, halfway - 1)[:, :N, :N]
            full_topo[halfway:, :N, :N] = topo_random_sampler(N=N, num_samples=num_permutations - halfway, type='mix',
                                                              with_replacement=False, check_for=full_topo[:halfway, :N, :N])
        else:
            full_topo[1:, :N, :N] = topo_random_sampler(N=N, num_samples=num_random, type='mix',
                                                        with_replacement=False, check_for=A[:N, :N])

        # Simulating events (divide time into chunks and randomly distribute each one)
        # Simulating approximately 'event_per_amp' events per internal (excluding CMC) amp
        num_events = E * events_per_edge
        event_length = T // num_events

        # Generating the affected modems matrix
        affected = torch.zeros((M, D, T))
        for i in range(num_events):
            t0 = i * event_length
            t1 = (i + 1) * event_length

            # Sampling a random internal amplifier or none at all!
            # We also want some not-affected regions
            if torch.rand(1)[0] < no_event_prop:
                continue
            num_faults = 2 if torch.rand(1)[0] < prop_multi else 1  # Potentially sampling multiple faults
            affected_amps = (torch.randperm(N - 1) + 1)[:num_faults]
            for affected_amp in affected_amps:
                affected_modems = [x - N for x in get_child_modems(affected_amp, A)]
                affected[affected_modems, :, t0:t1] = num_faults

        # Simulating the time series for each modem
        X = torch.stack([base_signal(D=D, T=T, phi_1=cfg.sim.phi_1, phi_2=cfg.sim.phi_2,
                                     mu_t=affected[i, :] * torch.tensor(cfg.sim.deviations)[:, None],
                                     sigma_t=cfg.sim.sigma) for i in range(M)])

        return X, full_topo, affected
        # observations.append(X)
        # affections.append(affected)

    if parallellize:
        results = Parallel(n_jobs=6)(delayed(simulate)(i) for i in range(num_samples))
    else:
        output = []
        for i in range(num_samples):
            output.append(simulate(i))

    for x, topo, affected in results:
        observations.append(x)
        topologies.append(topo)
        affections.append(affected)

    savepath = cfg.dir.data if savepath is None else savepath
    # Saving all
    save_pickle(savepath.joinpath(f"{N}_{events_per_edge}_time_series{'' if add_name is None else '_' + add_name}"),
                observations)
    save_pickle(savepath.joinpath(f"{N}_{events_per_edge}_affected{'' if add_name is None else '_' + add_name}"),
                affections)
    save_pickle(savepath.joinpath(f"{N}_{events_per_edge}_topologies{'' if add_name is None else '_' + add_name}"),
                topologies)
    return observations, topologies, affections


def simulate_general_observations(N_min=4, N_max=100, events_per_edge=None, no_event_prop=None, prop_multi=0,
                                  savepath=None, add_name=None, chunk_len=None, num_samples=None,
                                  _type='smart', random_multiplier=3, random_samples=None, parallellize=True):
    """
    General (multi-size tree) simulator
    The simulating function that simulates time series and adds events according to the number of events per amp at
    random. The time series is chopped into a number of blocks where each block either has an event (multifault at rate
    prop_multi) or not.
    The type is the type of sampling. 'full' gives the full topology set. If name is either 'random' or 'smart',
    num_random observations will be generated. 'random' is random, while 'smart' is based on the true topology and
    sampling from it.
    """
    no_event_prop = cfg.sim.no_event_prob if no_event_prop is None else no_event_prop
    num_samples = cfg.sim.num_samples if num_samples is None else num_samples
    events_per_edge = cfg.sim.num_events_per_amp if events_per_edge is None else events_per_edge
    Ns = torch.randint(N_min, N_max, (num_samples,))
    Es = Ns - 1
    D = cfg.sim.D
    # For a constant chunk length, T will be defined from the N and events_per_amp
    if chunk_len is None:
        Ts = torch.fill(torch.empty(num_samples), cfg.sim.T)
    else:
        Ts = Es * events_per_edge * chunk_len

    As = []
    for N in tqdm(Ns):
        As.append(topo_random_sampler(int(N), num_samples=1, type='mix'))
    observations, topologies, affections = [], [], []

    print(f"{f' Simulating observations for trees of size in the range [{N_min}, {N_max}] with {events_per_edge} mean events pr amp ':-^100s}")

    def simulate(num):
        N = int(Ns[num])
        E = int(Es[num])
        T = int(Ts[num])
        num_permutations = N * random_multiplier + 1 if random_samples is None else random_samples + 1
        if num % 10 == 0:
            print(f"{num} of {num_samples}")
        #print(num, N, T, num_permutations)
        # Simulate base signal and a number of modem children
        M = torch.randint((N - 1) * 2, (N - 1) * 6, (1,))[0]  # Between 2 and 5 modems per amp

        # Generating the Adjacency matrix and sampling a random permutation
        A = torch.zeros((M + N, M + N))
        A[:N, :N] = As[num]
        # We want a minimum of 2 modems per amplifier
        num_per_amp = torch.concat((torch.tensor([0]),
                                    torch.cumsum(
                                        torch.bincount(
                                            torch.randint(0, N - 1, (M - (N - 1) * cfg.sim.min_modems_per_amp,)),
                                            minlength=N - 1) + cfg.sim.min_modems_per_amp, 0))) + N
        for j in range(N - 1):
            A[j + 1, num_per_amp[j]:num_per_amp[j + 1]] = 1
        # Generating the full topology tensor

        if num_permutations >= (N ** (N - 2)):
            # full topos
            full_topo = A.repeat((int(N ** (N - 2)), 1, 1))
            all_poss = load_permutations(int(N))
            full_topo[1:, :N, :N] = all_poss[[i for i in range(N ** (N - 2)) if not torch.equal(all_poss[i], full_topo[0, :N, :N])]]
        else:
            full_topo = A.repeat((num_permutations, 1, 1))
            if _type == 'smart':
                full_topo[:, :N, :N] = topo_smart_sampler(A, num_permutations - 1)[:, :N, :N]
            elif _type == 'mix':
                halfway = num_permutations // 2 + num_permutations % 2
                full_topo[:halfway, :N, :N] = topo_smart_sampler(A, halfway - 1)[:, :N, :N]
                full_topo[halfway:, :N, :N] = topo_random_sampler(N=N, num_samples=num_permutations - halfway,
                                                                  type='mix', with_replacement=False, check_for=full_topo[:halfway, :N, :N])
            else:
                full_topo[1:, :N, :N] = topo_random_sampler(N=N, num_samples=num_permutations - 1, type='mix',
                                                            with_replacement=False, check_for=A[:N, :N])

        # Simulating events (divide time into chunks and randomly distribute each one)
        # Simulating approximately 'event_per_amp' events per internal (excluding CMC) amp
        num_events = E * events_per_edge
        event_length = T // num_events

        # Generating the affected modems matrix
        affected = torch.zeros((M, D, T))
        for i in range(num_events):
            t0 = i * event_length
            t1 = (i + 1) * event_length

            # Sampling a random internal amplifier or none at all!
            # We also want some not-affected regions
            if torch.rand(1)[0] < no_event_prop:
                continue
            num_faults = 2 if torch.rand(1)[0] < prop_multi else 1  # Potentially sampling multiple faults
            affected_amps = (torch.randperm(N - 1) + 1)[:num_faults]
            for affected_amp in affected_amps:
                affected_modems = [x - N for x in get_child_modems(affected_amp, A)]
                affected[affected_modems, :, t0:t1] = num_faults

        # Simulating the time series for each modem
        X = torch.stack([base_signal(D=D, T=T, phi_1=cfg.sim.phi_1, phi_2=cfg.sim.phi_2,
                                     mu_t=affected[i, :] * torch.tensor(cfg.sim.deviations)[:, None],
                                     sigma_t=cfg.sim.sigma) for i in range(M)])
        return X, full_topo, affected

    if parallellize:
        results = Parallel(n_jobs=6)(delayed(simulate)(i) for i in range(num_samples))
    else:
        output = []
        for i in range(num_samples):
            output.append(simulate(i))

    for x, topo, affected in results:
        observations.append(x)
        topologies.append(topo)
        affections.append(affected)

    savepath = cfg.dir.data if savepath is None else savepath
    # Saving all
    save_pickle(savepath.joinpath(f"_{events_per_edge}_time_series{'' if add_name is None else '_' + add_name}"),
                observations)
    save_pickle(savepath.joinpath(f"_{events_per_edge}_affected{'' if add_name is None else '_' + add_name}"),
                affections)
    save_pickle(savepath.joinpath(f"_{events_per_edge}_topologies{'' if add_name is None else '_' + add_name}"),
                topologies)
    return observations, topologies, affections


def parsimony_uniqueness_checker(As, As_check=None, num_check=None, parallellize=True, verbose=True):
    """
    Checking if all
    A should be a stack of adjacency matrices (3-way) with the same number of internal nodes N

    """
    num_samples = As.shape[0]
    N = (As[0].sum(axis=1) > 0).sum()
    M = As.shape[1] - N
    if As_check is None:
        idx = torch.arange(num_samples)
        As_check = As
    else:
        idx = \
            torch.where(
                (torch.swapaxes(As_check.repeat(num_samples, 1, 1, 1), 0, 1) == As[None, :, :, :]).all(-1).all(-1))[
                1]

    check_samples = len(As_check) if num_check is None else num_check

    def process(tt):
        if (tt % 10) == 0 and verbose:
            print(f"{tt} of {check_samples}")
        results = torch.zeros((num_samples, N - 1))
        for event in range(N - 1):
            X = torch.zeros((M, 1))
            affected_modems = [int(x - N) for x in get_child_modems(event + 1, As_check[tt])]
            X[affected_modems] = 1
            for ft in range(num_samples):
                results[ft, event] = parsimony(X, As[ft], N=N, normalize=False)
        return results

    if parallellize:
        output = Parallel(n_jobs=6)(delayed(process)(i) for i in range(check_samples))
    else:
        output = []
        for i in range(check_samples):
            output.append(process(i))
    return torch.stack(output), idx


def parsimony_uniqueness_smart(As_checker, num_random, verbose=True, parallellize=True):
    """
    Parsimony checker using smart sampling. The true topology will be the first one in either case.

    """
    num_samples = As_checker.shape[0]
    N = (As_checker[0].sum(axis=1) > 0).sum()
    M = As_checker.shape[1] - N

    def process(tt):
        if (tt % 10) == 0 and verbose:
            print(f"{tt} of {num_samples}")
        results = torch.zeros((num_random + 1, N - 1))
        As_random = topo_smart_sampler(As_checker[tt], num_random)
        for event in range(N - 1):
            X = torch.zeros((M, 1))
            affected_modems = [int(x - N) for x in get_child_modems(event + 1, As_checker[tt])]
            X[affected_modems] = 1
            for ft in range(num_random + 1):
                results[ft, event] = parsimony(X, As_random[ft], N=N, normalize=False)
        return results

    if parallellize:
        output = Parallel(n_jobs=6)(delayed(process)(i) for i in range(num_samples))
    else:
        output = []
        for i in range(num_samples):
            output.append(process(i))
    return torch.stack(output)
