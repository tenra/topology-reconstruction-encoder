import torch
import time

from tools.parser import load_permutations
from tools.simulator import parsimony_uniqueness_checker
import config

cfg = config.Config()

for N in [4, 5, 6]:  # 6 already takes a long time - ~30 minutes using 6 cores
    t1 = time.time()
    file_name = cfg.dir.conj_res.joinpath(f'N_{N}.txt')
    modems_per_amp = N - 1
    topos = load_permutations(N)
    num_perms = topos.shape[0]

    # Generating basic adjacency matrix for each topology
    A = torch.zeros((N + (N-1)*modems_per_amp, N + (N-1)*modems_per_amp))
    for i in range(N-1):
        A[i + 1, (N + i*modems_per_amp):(N + (i+1)*modems_per_amp)] = 1
    A = A.repeat((num_perms, 1, 1))
    A[:, :N, :N] = topos

    res, _ = parsimony_uniqueness_checker(A)

    # %% For how many is the real topology the unique minimum ?
    unique = 0
    for i in range(num_perms):
        sum_res = res[i].sum(axis=1)
        if sum_res.min() == sum_res[i]:
            if (sum_res == sum_res.min()).sum() == 1:
                unique += 1

    print(f"Testing trees of size {N}")
    print(f"Out of all {num_perms}, {unique} are the lowest and unique!\n\n")
    with open(file_name, 'w') as file:
        file.writelines([f"Tested all {num_perms} possible trees with {N} internal nodes\n",
                         f"Out of all {num_perms}, {unique} are the lowest and unique!\n",
                         f"Ran in {time.time() - t1} seconds."])
